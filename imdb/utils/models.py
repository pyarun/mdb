from django.db import models

# Create your models here.

class Language(models.Model):
    code = models.CharField(max_length=5)
    name = models.CharField(max_length=20)