from django.contrib import admin
from articles.models import Article, Certificate, Genre, IndustryType

# Register your models here.
admin.site.register(Article)
admin.site.register(Certificate)
admin.site.register(Genre)
admin.site.register(IndustryType)
