# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('utils', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Article',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=256, db_index=True)),
                ('sub_title', models.CharField(max_length=256, null=True, blank=True)),
                ('summary', models.TextField()),
                ('release_date', models.DateTimeField(db_index=True)),
                ('runtime', models.FloatField(help_text=b'play time of video', null=True, blank=True)),
                ('gross', models.FloatField(help_text=b'boxoffice collection')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Certificate',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('code', models.CharField(max_length=10)),
                ('name', models.CharField(max_length=50)),
                ('description', models.CharField(max_length=2000)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Genre',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=20)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='IndustryType',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='article',
            name='certificate',
            field=models.ForeignKey(blank=True, to='articles.Certificate', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='article',
            name='genres',
            field=models.ManyToManyField(to='articles.Genre'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='article',
            name='language',
            field=models.ForeignKey(to='utils.Language'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='article',
            name='whichwood',
            field=models.ForeignKey(to='articles.IndustryType'),
            preserve_default=True,
        ),
    ]
