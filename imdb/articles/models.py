from django.db import models

# Create your models here.

class Article(models.Model):
    """
    Maintain Videos information
    """
    title = models.CharField(max_length=256, db_index=True)
    sub_title = models.CharField(max_length=256, blank=True, null=True)
    summary = models.TextField()
    release_date = models.DateTimeField(db_index=True)
    runtime = models.FloatField(blank=True, null=True, help_text="play time of video")
    certificate = models.ForeignKey("Certificate", blank=True, null=True)
    language = models.ForeignKey("utils.Language")
    whichwood = models.ForeignKey("IndustryType")
    gross = models.FloatField(help_text="boxoffice collection")
    genres = models.ManyToManyField("Genre")
    
    
class Certificate(models.Model):
    """
    Certification from Movie Fedaration
    """
    code = models.CharField(max_length=10)
    name = models.CharField(max_length=50)
    description = models.CharField(max_length=2000)
    

class IndustryType(models.Model):
    """
    Which industry. ex: bollywood
    """
    name = models.CharField(max_length=50)
    
    
class Genre(models.Model):
    """
    Movie Genre
    """
    name = models.CharField(max_length=20)
    
    
